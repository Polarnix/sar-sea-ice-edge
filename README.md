SAR Sea Ice Edge is a set of Python scripts to generate sea ice edge information from synthetic aperture radar (SAR) satellite data such as Sentinel-1 available at the ESA Scientific Data Hub ([https://scihub.esa.int/](https://scihub.esa.int/)).

To see help on the parameters, run:
  
```
#!python

python generate_SAR_ice_edge.py --help
```

This shows the available options.


```
#!python
usage: generate_SAR_ice_edge.py [-h] [-d {0,1}] [-f {0,1}] [-o OUTDIR] filename

Process synthetic aperture radar (SAR) satellite images to generate a sea ice
edge and open water mask.

positional arguments:
  filename              SAR image filename

optional arguments:
  -h, --help            show this help message and exit
  -d {0,1}, --denoise {0,1}
                        Run denoise on S1 EW images [default: 0]
  -f {0,1}, --force {0,1}
                        Override limit to number of raw edge points [default:
                        0]
  -o OUTDIR, --outdir OUTDIR
                        Directory to download to [default: .]
```
The software uses the [Nansat Python library](https://github.com/nansencenter/nansat) produced by the [Nansen Environmental and Remote Sensing Center (NERSC)](https://www.nersc.no/) and, __**optionally**__, their Sentinel-1 EW mode image denoise routines:

Park, J. W., Korosov, A., & Babiker, M. (2017, April). Efficient thermal noise removal of Sentinel-1 image and its impacts on sea ice applications. In *EGU General Assembly Conference Abstracts* (Vol. 19, p. 12613).

In addition, coastline data from the [NOAA Global Self-consistent, Hierarchical, High-resolution Geography Database (GSHHG)](https://www.ngdc.noaa.gov/mgg/shorelines/gshhs.html) is used to generate a landmask for the SAR image.  A subset of this data is provided with the software for test purposes.

Wessel, P., and Smith, W.H.F. (1996). A Global Self-consistent, Hierarchical, High-resolution Shoreline Database, *J. Geophys. Res.*, 101, #B4, pp. 8741-8743, 1996. 

**Citing this software**  
This software has DOI: 10.5281/zenodo.818229 on Zenodo.  
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.818229.svg)](https://doi.org/10.5281/zenodo.818229)
