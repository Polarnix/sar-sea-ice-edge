#!/usr/bin/python

# Name:          alpha_shape.py
# Purpose:       Construct alpha shape (a.k.a. concave hull) around a set of points
#                Based on example from http://blog.thehumangeo.com/2014/05/12/drawing-boundaries-in-python/
# Author(s):     Nick Hughes
# Created:       2017-vi-26
# Modifications: 20??-??-?  - ?
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.818229
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys

import fiona
import shapely.geometry as geometry

from shapely.ops import cascaded_union, polygonize
from scipy.spatial import Delaunay
import numpy as N
import math

import osgeo.ogr as ogr
import osgeo.osr as osr


def alpha_shape(points, alpha):
    """
    Compute the alpha shape (concave hull) of a set
    of points.
    @param points: Iterable container of points.
    @param alpha: alpha value to influence the
        gooeyness of the border. Smaller numbers
        don't fall inward as much as larger numbers.
        Too large, and you lose everything!
    """
    if len(points) < 4:
        # When you have a triangle, there is no sense
        # in computing an alpha shape.
        return geometry.MultiPoint(list(points))
        #        .convex_hull
    def add_edge(edges, edge_points, coords, i, j):
        """
        Add a line between the i-th and j-th points,
        if not in the list already
        """
        if (i, j) in edges or (j, i) in edges:
            # already added
            return
        edges.add( (i, j) )
        edge_points.append(coords[ [i, j] ])
        
    coords = N.array([point.coords[0]
                       for point in points])
    tri = Delaunay(coords)
    edges = set()
    edge_points = []
    # loop over triangles:
    # ia, ib, ic = indices of corner points of the
    # triangle
    for ia, ib, ic in tri.vertices:
        pa = coords[ia]
        pb = coords[ib]
        pc = coords[ic]
        # Lengths of sides of triangle
        a = math.sqrt((pa[0]-pb[0])**2 + (pa[1]-pb[1])**2)
        b = math.sqrt((pb[0]-pc[0])**2 + (pb[1]-pc[1])**2)
        c = math.sqrt((pc[0]-pa[0])**2 + (pc[1]-pa[1])**2)
        # Semiperimeter of triangle
        s = (a + b + c)/2.0
        # Area of triangle by Heron's formula
        area = math.sqrt(s*(s-a)*(s-b)*(s-c))
        circum_r = a*b*c/(4.0*area)
        # Here's the radius filter.
        #print circum_r
        if circum_r < 1.0/alpha:
            add_edge(edges, edge_points, coords, ia, ib)
            add_edge(edges, edge_points, coords, ib, ic)
            add_edge(edges, edge_points, coords, ic, ia)
    m = geometry.MultiLineString(edge_points)
    triangles = list(polygonize(m))
    return cascaded_union(triangles), edge_points


# Routine to generate polygons from cluster points input
def generate_polygons( cluster_pts, cluster_ids, shpfn, debug ):
    
    if debug > 0:
        print '\n   Generating alpha shape (a.k.a. concave hull) polygons from clusters of points.'

    # Generate Shapely geometries
    # print 'here1', len(cluster_pts)
    points = []
    for pt in cluster_pts:
        # print pt
        points.append( geometry.Point(pt[1],pt[0]) )
        # print geometry.Point(pt[1],pt[0])
        # sys.exit()
    attribs = N.array( cluster_ids, dtype=N.uint16 )
    # print 'here2'

    # Set alpha coefficient
    alpha = 5.0

    # Create Shapefile for output
    if debug > 0:
        shpdrv = ogr.GetDriverByName('ESRI Shapefile')
        outfn = shpfn.replace('_lines','_point_cluster_polygons')
        print outfn        
        if os.path.exists(outfn):
            shpdrv.DeleteDataSource(outfn)
        out_ds = shpdrv.CreateDataSource(outfn)
        ll_srs = osr.SpatialReference()
        ll_srs.ImportFromEPSG(4326)
        layer_name = "point_cluster_polygons"
        out_lay = out_ds.CreateLayer(layer_name, ll_srs, geom_type=ogr.wkbPolygon)
        idField = ogr.FieldDefn("cluster", ogr.OFTInteger)
        out_lay.CreateField(idField)
        featureDefn = out_lay.GetLayerDefn()
        
    # Blank list for polygons
    polylist = []
    
    # Loop through cluster point sets and generate polygons
    cluster_ids = N.unique(attribs)
    for cluster_id in cluster_ids:
        idx = N.nonzero(attribs == cluster_id)
        if debug > 0:
            print cluster_id, len(idx[0])
        new_points = [points[i] for i in idx[0]]
        # print len(new_points)
        concave_hull, edge_points = alpha_shape( new_points, alpha=alpha )
        # print len(edge_points)
        # for j in range(len(edge_points)):
        # m    print edge_points[j]
        # print concave_hull

        # Get polygon WKT
        wkt = concave_hull.wkt
        polylist.append( wkt )

        # Add to Shapefile
        if debug > 0:
            poly = ogr.CreateGeometryFromWkt(wkt)
            out_feat = ogr.Feature(featureDefn)
            out_feat.SetGeometry(poly)
            out_feat.SetField("cluster", int(cluster_id) )
            out_lay.CreateFeature(out_feat)
            out_feat = None
            poly = None
        
    # Close Shapefile
    if debug > 0:
        out_ds = None
        
    return polylist
    

# Main program routine for testing
if __name__ == '__main__':
    shpfn = './S1A_EW_GRDM_1SDH_20170328T052617_20170328T052717_015888_01A303_97C1.SAFE_sigma0_denoised_icemask_gcps_lines_clusters.shp'
    shapefile = fiona.open( shpfn )
    points = [geometry.shape(point['geometry'])
        for point in shapefile]
    attribs = [point['properties']['cluster']
        for point in shapefile]
    print points[0], attribs[0]
    print len(points)
    attribs = N.array( attribs, dtype=N.uint16 )
    
    # Set alpha coefficient
    alpha = 10.0

    # Create Shapefile for output
    shpdrv = ogr.GetDriverByName('ESRI Shapefile')
    outfn = shpfn.replace('_lines_clusters','_polygons')        
    if os.path.exists(outfn):
        shpdrv.DeleteDataSource(outfn)
    out_ds = shpdrv.CreateDataSource(outfn)
    ll_srs = osr.SpatialReference()
    ll_srs.ImportFromEPSG(4326)
    layer_name = "cluster_polygons"
    out_lay = out_ds.CreateLayer(layer_name, ll_srs, geom_type=ogr.wkbPolygon)
    idField = ogr.FieldDefn("cluster", ogr.OFTInteger)
    out_lay.CreateField(idField)
    featureDefn = out_lay.GetLayerDefn()
    
    # Loop through cluster point sets and generate polygons
    cluster_ids = N.unique(attribs)
    for cluster_id in cluster_ids:
        idx = N.nonzero(attribs == cluster_id)
        print cluster_id, len(idx[0])
        new_points = [points[i] for i in idx[0]]
        print len(new_points)
        concave_hull, edge_points = alpha_shape( new_points, alpha=alpha )
        print len(edge_points)
        # for j in range(len(edge_points)):
        # m    print edge_points[j]
        # print concave_hull

        # Add to Shapefile
        wkt = concave_hull.wkt
        poly = ogr.CreateGeometryFromWkt(wkt)
        out_feat = ogr.Feature(featureDefn)
        out_feat.SetGeometry(poly)
        out_feat.SetField("cluster", int(cluster_id) )
        out_lay.CreateFeature(out_feat)
        out_feat = None
        poly = None
        
    # Close Shapefile
    out_ds = None
    