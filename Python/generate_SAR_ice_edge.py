#!/usr/bin/python

# Name:          generate_ice_edge.py
# Purpose:       Process synthetic aperture radar (SAR) satellite images to generate a sea ice edge
#                and open water mask.
# Author(s):     Nick Hughes
# Created:       2017-vi-26
# Modifications: 2017-vii-03 - Add output directory functionality.
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.818229
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys
import shutil
from datetime import datetime
from zipfile import ZipFile
import warnings
import argparse

import numpy as N

import osgeo.ogr as ogr

from nansat import Nansat

# Try to import Nansat S1 EW denoise routine
warnings.simplefilter("ignore")
HOMEDIR = os.path.expanduser("~")
sys.path.append( ("%s/git" % HOMEDIR) )
try:
    from sentinel1denoised.S1_EW_GRD_NoiseCorrection import Sentinel1Image
except:
    denoise = False
else:
    denoise = True
# warnings.simplefilter("default")
   

from generate_landmask import generate_landmask
from edge_detection import edge_detection
from raster2lines import raster2lines
from reduce2points import reduce2points
from snn_clustering import dbscan_cluster
import alpha_shape
from combine_masks import combine_masks

# Local settings
TMPDIR="../tmp"

# Channel sets for Sentinel-1
CHNS = { '1SDH': [ 'HH', 'HV' ], \
         '1SSH': [ 'HH' ], \
         '1SDV': [ 'VV', 'VH' ], \
         '1SSV': [ 'VV' ] }


# Process S1 EW mode images using Nansat (see example 00denoise.py in sentinel1ice)
def process_S1_EW( inpfn, s1pol, dnflg, outdir, debug ):

    rootfn = os.path.split( inpfn )[1]
    # print rootfn
    
    if debug > 0:
        results = {}
    
    # Open Sentinel-1 image using Nansat
    s1i = Sentinel1Image(inpfn)

    outbands = []    
    for pol in CHNS[s1pol]:

        bandname = ("sigma0_%s" % pol)

        if dnflg == True:

            # run denoising
            print 'Run denoising of sigma0_%s in %s' % (pol, rootfn)
            # print inpfn
            # print help(Sentinel1Image)
            s1i.add_denoised_band(bandname)
            denoised = ("sigma0_%s_denoised" % pol)
            print denoised
            outbands.append(denoised)

            if debug > 0:
                print 'Read denoised sigma0_%s from %s' % (pol, rootfn)
                results['sigma0_%s_denoised' % pol] = s1i['sigma0_%s_denoised' % pol]

                print 'Make full res JPG'
                jpgfile = ("%s/%s_%s.jpg" % (outdir, rootfn, pol))
                if not os.path.exists(jpgfile):
                    print jpgfile
                    vmin = N.percentile(results['sigma0_%s_denoised' % pol][
                                         N.isfinite(results['sigma0_%s_denoised' % pol])
                                         ], 1)
                    vmax = N.percentile(results['sigma0_%s_denoised' % pol][
                                         N.isfinite(results['sigma0_%s_denoised' % pol])
                                         ], 99)
                    s1i.write_figure(jpgfile, 'sigma0_%s_denoised' % pol, clim=[vmin, vmax], cmapName='gray')

        else:
            outbands.append(bandname)
            
    # Export to GeoTIFF
    tiffn = os.path.join( outdir, rootfn+'_sigma0_denoised.tif')
    print "Exporting to GeoTIFF", tiffn
    print "  Bands =", outbands
    s1i.export(tiffn,bands=outbands,addGCPs=True,driver='GTiff')

    # Close Sentinel-1 image
    # del s1i
    
    return tiffn


# Function to generate S1 GeoTIFF
if __name__ == '__main__':

    # Get user options
    parser = argparse.ArgumentParser(description='Process synthetic aperture radar (SAR) satellite images to generate a sea ice edge and open water mask.')
    parser.add_argument("filename", help="SAR image filename", type=str )
    parser.add_argument("-d", "--denoise", help="Run denoise on S1 EW images [default: %(default)s]", type=int, \
        choices=[0,1], default=1 )
    parser.add_argument("-f", "--force", help="Override limit to number of raw edge points [default: %(default)s]", type=int, \
        choices=[0,1], default=0 )
    parser.add_argument("-o", "--outdir", help="Directory to download to [default: %(default)s]", type=str, \
        default='.' )
    parser.add_argument("-v", "--verbose", help="Show additional debugging information [default: %(default)s]", type=int, \
        choices=[0,1,2], default=0 )

    args = parser.parse_args()
    # print args

    # Extract arguments and further check arguments
    inpfn = args.filename
    dnflg = args.denoise
    forceflg = args.force
    outdir = args.outdir
    debug = args.verbose

    # Check to see if TMPDIR directory exists and if not, create it
    if os.path.isdir(TMPDIR) == False:
        os.mkdir(TMPDIR)

    # Change denoise flag if it has been set
    if dnflg == 0 and denoise == 1:
        denoise = False
    # print dnflg, denoise
    
    # Split the S1 filename and get information    
    srcdir, srcfn = os.path.split( inpfn )
    rootfn = srcfn.replace('.zip','')
    # print rootfn
    fnbits = rootfn.split('_')
    s1sat = fnbits[0]
    s1mode = fnbits[1]
    s1prod = fnbits[2]
    s1pol = fnbits[3]
    s1dt = datetime.strptime(fnbits[4],'%Y%m%dT%H%M%S')
    # print s1dt
    
    # Extract files from zip-file
    zf = ZipFile( inpfn, 'r' )
    zf.extractall(TMPDIR)
    zf.close()
    
    # Define product directory
    proddir = ("%s/%s" % (TMPDIR,rootfn))
    
    # Process EW mode images with Nansat S1 denoising routine
    if s1mode == 'EW':
        tmpfn = process_S1_EW( proddir, s1pol, denoise, outdir, debug )
        # pass
    else:
        print 'Processing not defined yet.'
        sys.exit()
        
    # Delete extracted files 
    shutil.rmtree(proddir)
    
    # Ice edge extraction and generation of an OW mask should go here
    #   Needs land mask as well, so extract that from run_segmentation
    landmask_fn = generate_landmask( tmpfn, 'I', debug )

    # Clustering from http://geoffboeing.com/2014/08/clustering-to-reduce-spatial-data-set-size/
    # Concave hull from http://blog.thehumangeo.com/2014/05/12/drawing-boundaries-in-python/
    edgefn = edge_detection( tmpfn, landmask_fn, debug )
        
    # Number of CPU's for multiprocessing
    ncpu = 1

    # Convert edge file to Shapefile
    # Creating lines from a raster
    # http://gis.stackexchange.com/questions/78023/gdal-polygonize-lines
    pixel_values = [ 2, 8, 10 ]
    linesfn = raster2lines( edgefn, pixel_values, ncpu, debug )
    # print linesfn
    
    # Load point data and reduce for number of points 
    pixel_values = [ 8, 10 ]
    pts = reduce2points( linesfn, pixel_values, debug )

    # Check total available computer memory (see https://stackoverflow.com/questions/22102999/get-total-physical-memory-in-python)
    mem_bytes = os.sysconf('SC_PAGE_SIZE') * os.sysconf('SC_PHYS_PAGES')
    mem_gib = mem_bytes/(1024.**3)
    scalar = 3000000.0 / 32.0
    ptslimit = int(scalar * mem_gib)
    
    # Abort if we have too many points to process
    
    if len(pts) > ptslimit and forceflg == 0:
        print ("Too many (%d) points, limit is %d. Stopping." % (len(pts),ptslimit))
        err = open("too_many_points.txt", "a")
        err.write( ("%s %d\n" % (rootfn,len(pts))) )
        err.close()
        sys.exit()
    
    # Cluster point set into groups
    # Get root filename
    indir, infn = os.path.split(linesfn)
    rootfn = infn.replace('.shp','')
    print ("rootfn = %s" % rootfn)
    cluster_pts, cluster_ids = dbscan_cluster( pts, rootfn, debug )
    
    # Convert point groups to concave hull (alpha_shape)
    polygons = alpha_shape.generate_polygons( cluster_pts, cluster_ids,linesfn, debug )
    
    # Combine the masks
    pixel_values = [ 2 ]
    copol_pts = reduce2points( linesfn, pixel_values, debug )
    combined_mask_fn = combine_masks( landmask_fn, polygons, copol_pts, outdir, debug )           
        
    # Load OGR Shapefile driver
    shpdrv = ogr.GetDriverByName("ESRI Shapefile")

    # Clean up
    os.remove(landmask_fn)
    os.remove(edgefn)
    if os.path.exists(linesfn):
        shpdrv.DeleteDataSource(linesfn)
