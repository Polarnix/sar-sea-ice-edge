#!/usr/bin/python

# Name:          gshhs_landmask.py
# Purpose:       Routine to generate a landmask from GSHHS Shapefiles.
# Author(s):     Nick Hughes
# Created:       2017-vi-26
# Modifications: 20??-??-?  - ?
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.818229
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys
import subprocess

import numpy as N

import osgeo.gdal as gdal
from osgeo.gdalconst import *
from osgeo import ogr
import osgeo.osr as osr

from PIL import Image, ImageDraw


# Local settings
TMPDIR="../tmp"
GSHHSDIR='../Include'
GDALHOME='/usr/local/bin'


# Create a landmask
def gshhs_landmask( sarfn, detail, debug ):
        
    # Get root filename
    indir, infn = os.path.split(sarfn)
    rootfn = infn.replace('.tif','')
    
    # Open input file
    inds = gdal.Open( sarfn )
    transform = inds.GetGeoTransform()
    projstr = inds.GetProjection()
    gcps = inds.GetGCPs()

    # We want our mask file to have the same dimensions as the original image
    nrow = inds.RasterYSize
    ncol = inds.RasterXSize

    # Create coordinate transformation from image coords to lat/lon
    tr = gdal.Transformer(inds, None, ['METHOD=GCP_POLYNOMIAL'])

    # Open shapefile (assumes that polygons are in the first layer - true for GSHHS)
    shpfn = ("%s/GSHHS_%s_L1.shp" % (GSHHSDIR,detail.lower()))
    shp = ogr.Open(shpfn)
    layer = shp.GetLayer(0)
    nfeat = layer.GetFeatureCount()

    # Mask colour value
    maskcol = 128

    # Create initial mask image
    mask = Image.new('L', (ncol, nrow), 0)

    # Loop through all land polygons
    points = []
    for i in range(nfeat):
        # Get current polygon
        feat = layer.GetFeature(i)
        geometry = feat.GetGeometryRef()
        ring = geometry.GetGeometryRef(0)
        # npoint = ring.GetPointCount()

        # Loop through polygon points and convert lat/lon to pixel coordinates
        points = []
        for idx, p in enumerate(ring.GetPoints()):
            # The trick is this line. The first 1 asks the transformer to do an
            # inverse transformation, so this is transforms lat/lon coords to pixel coords
            (success, point) = tr.TransformPoint(1, p[0], p[1], 0)
            if success:
                px = point[0]
                py = point[1]
                points.append((px, py))

        # Rasterize the polygon in image coordinates.
        # The polygon function requires at least two points.
        if len(points) > 2:
            ImageDraw.Draw(mask).polygon(points, outline=maskcol, fill=maskcol)

    # Convert to a numpy array (not sure if this is necessary)
    mask = N.array(mask).astype(bool)

    # Write output to file
    driver = gdal.GetDriverByName('GTiff')
    driver.Register()
    tmpmaskfn = ("%s/%s_landmask_tmp.tif" % (TMPDIR,rootfn))
    createopts=[ "COMPRESS=LZW", "BIGTIFF=YES"]
    outds = driver.Create(tmpmaskfn, ncol, nrow, 1, gdal.GDT_Byte, createopts)
    if outds is None:
        print 'Could not create ', tmpmaskfn
        sys.exit()
    outband = outds.GetRasterBand(1)
    outband.WriteArray(mask)

    # Flush data to disk
    outband.FlushCache()

    # Georeference the image and set the projection
    outds.SetGeoTransform(transform)
    outds.SetProjection(projstr)

    # Close and clean up
    outds = None
    inds = None

    # Apply GCPs
    maskfn = ("%s/%s_landmask.tif" % (TMPDIR,rootfn))
    cmd = ("%s/gdal_translate -of GTiff" % GDALHOME)
    cmd = ("%s -a_srs \"+proj=longlat +ellps=WGS84\"" % cmd)
    if debug == 0:
        cmd = ("%s -quiet" % cmd)
    for gcp in gcps:
        cmd = ("%s -gcp %f %f %f %f %f" \
            % (cmd,gcp.GCPPixel,gcp.GCPLine,gcp.GCPX,gcp.GCPY,gcp.GCPZ))
    cmd = ("%s %s %s" % (cmd,tmpmaskfn,maskfn))
    # print cmd
    try:
        retcode = subprocess.call(cmd, shell=True)
        if retcode < 0:
            print >>sys.stderr, "Child was terminated by signal", -retcode
    except OSError, e:
        print >>sys.stderr, "Execution failed:", e

    # Remove temporary file
    os.remove( tmpmaskfn )

    return maskfn
