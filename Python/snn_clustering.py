#!/usr/bin/python

# Name:          snn_clustering.py
# Purpose:       Shared Nearest Neighbour (SNN) clustering, in this case using DBSCAN.
#                See example at http://geoffboeing.com/2014/08/clustering-to-reduce-spatial-data-set-size/
# Author(s):     Nick Hughes
# Created:       2017-vi-26
# Modifications: 20??-??-?  - ?
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.818229
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys

import numpy as N
from sklearn.cluster import DBSCAN
import pandas as pd

import osgeo.ogr as ogr
import osgeo.osr as osr

# Local settings
TMPDIR="../tmp"


# Density (DBSCAN) clustering) to get main areas and remove outliers
# Takes points as list of latitude,longitude pairs
def dbscan_cluster( ptlist, rootfn, debug ):
    
    if debug > 0:
        print '\n   Clustering points into spatially separated groups using DBSCAN.'

    # Geospatial and clustering parameters
    kms_per_radian = 6371.0088
    epsilon = 3.0 / kms_per_radian
    minsamp = 150
    
    # Perform clustering
    coords = N.array( ptlist, dtype=N.float32 )
    db = DBSCAN(eps=epsilon, min_samples=minsamp, algorithm='ball_tree', metric='haversine').fit(N.radians(coords))

    # Get cluster data
    cluster_labels = db.labels_
    num_clusters = len(set(cluster_labels))
    clusters = pd.Series([coords[cluster_labels == n] for n in range(num_clusters)])
    if debug > 0:
        print('Number of clusters: {}'.format(num_clusters))
    
    # Create cluster_list
    cluster_list = []
    cluster_ids = []
    for i in range(num_clusters):
        np = len(clusters[i])
        ptdata = clusters[i]
        for j in range(np):
            cluster_list.append( [ ptdata[j][0], ptdata[j][1] ] )
            cluster_ids.append( i )
    # print cluster_list

    # Output clusters Shapefile
    if debug > 1:
        shpfn = ("%s/%s.shp" % (TMPDIR,rootfn))
        shpfn = shpfn.replace('_lines','_points_clustered')
        print shpfn
        shpdrv = ogr.GetDriverByName('ESRI Shapefile')
        if os.path.exists(shpfn):
            shpdrv.DeleteDataSource(shpfn)
        out_ds = shpdrv.CreateDataSource(shpfn)
        ll_srs = osr.SpatialReference()
        ll_srs.ImportFromEPSG(4326)
        layer_name = ("%s_clusters" % rootfn)
        out_lay = out_ds.CreateLayer(layer_name, ll_srs, geom_type=ogr.wkbPoint)
        idField = ogr.FieldDefn("cluster", ogr.OFTInteger)
        out_lay.CreateField(idField)
        featureDefn = out_lay.GetLayerDefn()
        for i in range(num_clusters):
            np = len(clusters[i])
            ptdata = clusters[i]
            for j in range(np):
                pt = ogr.Geometry(ogr.wkbPoint)
                pt.AddPoint_2D( float(ptdata[j][1]), float(ptdata[j][0]) )
                out_feat = ogr.Feature(featureDefn)
                out_feat.SetGeometry(pt)
                out_feat.SetField("cluster", (i+1))
                out_lay.CreateFeature(out_feat)
                out_feat = None
                pt = None
        out_ds = None

    return [ cluster_list, cluster_ids ]    