#!/usr/bin/python

# Name:          memory_usage_psutil.py
# Purpose:       Get memory of current process in Mb.
#                NB: Earlier versions of psutil used get_memory_info rather than memory_info.
# Author(s):     Nick Hughes
# Created:       2017-vi-26
# Modifications: 20??-??-?  - ?
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.818229
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.


import os
import psutil

# Return the memory usage in MB
def memory_usage_psutil():
    process = psutil.Process(os.getpid())
    mem = process.memory_info()[0] / float(2 ** 20)
    return mem

