#!/usr/bin/python

# Name:          raster2lines.py
# Purpose:       Creating lines from a raster.
#                Based on examples from
#                  http://gis.stackexchange.com/questions/78023/gdal-polygonize-lines
#                and
#                  http://pcjericks.github.io/py-gdalogr-cookbook/raster_layers.html#raster-to-vector-line
# Author(s):     Nick Hughes
# Created:       2017-vi-26
# Modifications: 20??-??-?  - ?
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.818229
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys

import numpy as N

import osgeo.gdal as gdal
import osgeo.ogr as ogr
import osgeo.osr as osr

import shapely.ops
import shapely.wkt

import itertools
from math import sqrt,ceil

from multiprocessing import Pool, TimeoutError

from memory_usage_psutil import memory_usage_psutil


# Local settings
TMPDIR="../tmp"

# Outputs directory
OUTDIR = TMPDIR


# Convert raster to Shapefile linestrings
def raster2lines( inpfn, pixel_values, ncpu, debug ):

    if debug > 0:
        print '\n   Extracting line segments from edge detected image.'
        print ("\nMemory: %7.2f" % memory_usage_psutil())

    # Get root filename
    indir, infn = os.path.split(inpfn)
    rootfn = infn.replace('.tif','')

    # Set up GeoTIFF driver
    tifdrv = gdal.GetDriverByName('GTiff')

    # Define step size for loading data (500 seems to be OK for problem datasets and keeps memory below 5 GB)
    stepsz = 500

    # Load the raster image as an array
    in_ds = gdal.Open( inpfn, 0 )
    # geotransform = in_ds.GetGeoTransform()
    # in_srs_wkt = in_ds.GetProjectionRef()
    in_band = in_ds.GetRasterBand(1)

    # Get image size information
    xsz = in_band.XSize
    ysz = in_band.YSize

    # Maximum distance between points
    if rootfn.find('RS2') > -1:
        sattype = 'RS2'
        pixel_width = 75.0
    elif rootfn.find('S1A') > -1 or rootfn.find('S1B') > -1:
        sattype = 'S1'
        pixel_width = 75.0
    else:
        print 'Unknown satellite image type.'
        sys.exit()
    if debug > 0:
        print ("Satellite image type: %s" % sattype)

    # Get region for projection        
    in_gcps = in_ds.GetGCPs()
    if in_gcps[0].GCPY > 30.0:
        region = 'arctic'
        psorig = '90n'
    elif in_gcps[0].GCPY < -30.0:
        region = 'antarctic'
        psorig = '90s'
    else:
        print ("Latitude is %f. Unhandled region in low latitudes." % in_gcps[0].GCPY)
        sys.exit()

    # pixel_width = geotransform[1]
    max_distance = ceil(sqrt(2*pixel_width*pixel_width))
    if debug > 0:
        print ("max_distance: %.2f" % max_distance)

    # Define input and output spatial reference systems
    proj4str = ("+proj=stere +lat_0=%s +lon_0=0e +lat_ts=%s +ellps=WGS84 +datum=WGS84" \
        % (psorig,psorig))
    in_srs = osr.SpatialReference()
    in_srs.ImportFromEPSG(4326)
    out_srs = osr.SpatialReference()
    out_srs.ImportFromProj4( proj4str )
    ll2ps = osr.CoordinateTransformation( in_srs, out_srs )

    # Create coordinate transformation from image coords to lat/lon
    # See http://www.gdal.org/gdal__alg_8h.html#a94cd172f78dbc41d6f407d662914f2e3 for options
    dst_srs_txt = ("DST_SRS=%s" % out_srs.ExportToWkt())
    tr = gdal.Transformer(in_ds, None, ['METHOD=GCP_POLYNOMIAL', 'MAX_GCP_ORDER=3', \
        dst_srs_txt])
    # 'MAX_GCP_ORDER=2',

    # Open Shapefile for writing
    shpdrv = ogr.GetDriverByName("ESRI Shapefile")
    shpfn = ("%s/%s_lines.shp" % (OUTDIR,rootfn))
    if os.path.exists(shpfn):
        shpdrv.DeleteDataSource(shpfn)
    out_ds = shpdrv.CreateDataSource(shpfn)
    out_lay = out_ds.CreateLayer( ("%s_lines" % rootfn), srs=out_srs, geom_type=ogr.wkbMultiLineString )
    out_lay.CreateField(ogr.FieldDefn("value", ogr.OFTInteger))
    out_featdefn = out_lay.GetLayerDefn()

    if debug > 0:
        print ("Calculating ice edge vectors\n")
        print "    #  strx  endx  stry  endy pix xcount ycount  Memory Combinations  Valid"

    # Loop through pixel values
    for pixval in pixel_values:
        if debug > 0:
            print ("\npixval = %d " % pixval)

        # Loop through blocks of data
        xblocks = int( xsz / stepsz ) + 1
        yblocks = int( ysz / stepsz ) + 1
        if debug > 0:
            print 'Total blocks =', (xblocks * yblocks)
        stry = 0
        blocki = 0
        whileendy = ysz
        # print whileendy

        # New multiline geometry
        # multiline = ogr.Geometry(ogr.wkbMultiLineString)
        multiline = []

        # Loop through block rows
        # while stry < 1000:
        while stry < whileendy:
            endy = stry + stepsz
            if endy > ysz:
                endy= ysz
            ycount = (endy - stry)

            # Reset x counters
            strx = 0
            whileendx = xsz

            # Loop through block columns
            # while strx < 1000:
            while strx < whileendx:
                endx = strx + stepsz
                if endx > xsz:
                    endx = xsz
                xcount = (endx - strx)

                # Load data
                in_data = in_band.ReadAsArray(strx,stry,xcount,ycount).astype(N.uint8)

                # Convert array to a dictionary
                edgelist = N.where( in_data == pixval )
                if len(edgelist[0]) > 1:
                    # multipoint = ogr.Geometry(ogr.wkbMultiLineString)
                    count = 0
                    if ncpu == 1:
                        pointlist = []
                        for indexy in edgelist[0]:
                            indexx = edgelist[1][count]
                            # xcoord, ycoord = pixel2coordinate2(tr,ll2ps,indexx+strx,indexy+stry)
                            (success,pscoord) = tr.TransformPoint(0, indexx+strx, indexy+stry, 0)
                            # pointlist.append( [xcoord,ycoord] )
                            pointlist.append( [pscoord[0],pscoord[1]] )
                            count = count + 1
                        pointarray = N.array(pointlist,dtype=N.float32)
                        # print pointarray.shape
                        
                    # Points to wkbMultiLineString
                    ndict = len(pointlist)
                    combinations = (ndict * ndict) / 2
                    # print '  ', count, ndict, combinations

                    # Single processor version
                    if ncpu == 1:
                        distgrid = N.zeros( (ndict,ndict), dtype=N.float32)
                        for i in range(ndict):
                            # pt1 - pt2
                            xval = pointarray[i:ndict,0] - pointarray[i,0]
                            yval = pointarray[i:ndict,1] - pointarray[i,1]
                            distgrid[i,i:ndict] = N.sqrt( (xval*xval) + (yval*yval) )
                        magrid = N.ma.masked_equal( distgrid, 0.0 )
                        coords = N.nonzero(magrid<max_distance)
                        # print coords
                        idx = 0
                        for i in range(len(coords[0])):
                            pt1ref = coords[0][i]
                            pt2ref = coords[1][i]

                            # Create a line segment (Shapely)
                            shapely_line = ( (float(pointarray[pt1ref,0]),float(pointarray[pt1ref,1])), \
                                (float(pointarray[pt2ref,0]),float(pointarray[pt2ref,1])) )
                            multiline.append( shapely_line )
                                
                            idx = idx + 1
                            
                    elif ncpu > 1:
                        pool = Pool(processes=ncpu)    # Start however many CPU processes we want

                    else:
                        print "Cannot have less than 1 CPU!"
                        sys.exit()

                    if debug > 0:
                        print ("%5d %5d %5d %5d %5d %3d %6d %6d %7.2f %12d %6d" % \
                            (blocki, strx, endx, stry, endy, pixval, xcount, ycount, memory_usage_psutil(), \
                            combinations, idx))

                blocki = blocki + 1
                strx = endx

            stry = endy
            # print 'stry', stry

        if debug > 0:
            print ("multiline Memory: %7.2f" % memory_usage_psutil())

        # Use Shapely to line merge
        # print len(multiline)
        if len(multiline) > 0:
            outlines = shapely.ops.linemerge(multiline)
            
            for outline in outlines:
                outwkt = shapely.wkt.dumps(outline)
                outgeom = ogr.CreateGeometryFromWkt(outwkt)
                out_feat = ogr.Feature(out_featdefn)
                out_feat.SetField("value", pixval)
                out_feat.SetGeometry(outgeom)
                out_lay.CreateFeature(out_feat)
                out_feat.Destroy()

    # Close the output Shapefile
    out_ds.Destroy()

    # Close the input file
    in_ds = None

    return shpfn
