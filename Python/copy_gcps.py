#!/usr/bin/python

# Name:          copy_gcps.py
# Purpose:       Copy Ground Control Point (GCP) data from one GeoTIFF to another.
# Author(s):     Nick Hughes
# Created:       2017-vi-26
# Modifications: 20??-??-?  - ?
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.818229
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.


import osgeo.gdal as gdal

# Copy GeoTIFF GCP points
def copy_gcps( inds, outds ):
    gcps = inds.GetGCPs()
    new_gcps = []
    for gcp in gcps:
        ngcp = gdal.GCP()
        ngcp.GCPX = gcp.GCPX
        ngcp.GCPY = gcp.GCPY
        ngcp.GCPZ = gcp.GCPZ
        ngcp.GCPPixel = gcp.GCPPixel
        ngcp.GCPLine = gcp.GCPLine
        new_gcps.append(ngcp)
    outds.SetGCPs( new_gcps, inds.GetGCPProjection() )

    return
