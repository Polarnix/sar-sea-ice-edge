#!/usr/bin/python

# Name:          reduce2points.py
# Purpose:       Routine to reduce line segments to a filtered set of points.
# Author(s):     Nick Hughes
# Created:       2017-vi-26
# Modifications: 20??-??-?  - ?
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.818229
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys

import osgeo.ogr as ogr
import osgeo.osr as osr

import numpy as N


# Local settings
TMPDIR="../tmp"


# Reduce Numpy array to unqiue rows
# See http://stackoverflow.com/questions/31097247/remove-duplicate-rows-of-a-numpy-array
def unique_rows(a):
    a = N.ascontiguousarray(a)
    unique_a = N.unique(a.view([('', a.dtype)]*a.shape[1]))
    return unique_a.view(a.dtype).reshape((unique_a.shape[0], a.shape[1]))


# Reduce to points
def reduce2points( shpfn, target_values, debug ):
    
    if debug > 0:
        print '\n   Reducing lines to set of unique points.'

    # Get root filename
    indir, infn = os.path.split(shpfn)
    rootfn = infn.replace('.shp','')
    
    # LOad Shapefile driver
    shpdrv = ogr.GetDriverByName('ESRI Shapefile')
    
    # Open input dataset
    in_ds = shpdrv.Open(shpfn, 0)
    if in_ds is None:
        print 'Could not open %s' % (shpfn)
        sys.exit()
    else:
        print 'Opened %s' % (shpfn)
    in_lay = in_ds.GetLayer()
    nfeat = in_lay.GetFeatureCount()
    in_srs = in_lay.GetSpatialRef()
    if debug > 0:
        print "Number of features in %s: %d" % (os.path.basename(shpfn),nfeat)
        print 'Projection:', in_srs.ExportToWkt()

    # Set up longitude/latitude map projection and transform
    ll_srs = osr.SpatialReference()
    ll_srs.ImportFromEPSG(4326)
    in2ll = osr.CoordinateTransformation( in_srs, ll_srs )

    # Blank list for points
    pt_list = []

    # Iterate over features
    for i in range(nfeat):
        if debug > 0 and i % 100000 == 0:
            print ("%8d" % i)
        in_feat = in_lay.GetFeature(i)
        in_geom = in_feat.GetGeometryRef()
        in_val = in_feat.GetField("value")
        # print ("%8d %1d %s" % (i,in_val,in_geom.ExportToWkt()))
        if in_val in target_values:
            npoint = in_geom.GetPointCount()
            # print npoint
            for j in range(npoint):
                in_point = in_geom.GetPoint(j)
                # print in_point
                pt_list.append( tuple( [ in_point[0], in_point[1] ] ) )
            # print pt_list

    # Convert point list to Numpy array
    #    444529 rows
    tmp_array = N.array( pt_list, dtype=N.float32 )
    pt_array = unique_rows(tmp_array)
    if debug > 0:
        print 'Data array shapes:'
        print 'Original -', tmp_array.shape
        print 'Final    -', pt_array.shape

    # Output longitude/latitude data in CSV format for sklearn_example.py
    # Also create list of latitude,longitude pairs for DBSCAN processing
    np, ncol = pt_array.shape
    outlist = []
    # Create CSV-file for debugging
    if debug > 0:
        csvfn = ("%s/%s.csv" % (TMPDIR,rootfn))
        csvfn = csvfn.replace('_lines','_points')
        print csvfn
        fout = open( csvfn, 'w' )
        fout.write( "lon,lat\n" )
    
    # Loop through points
    for i in range(np):
        psx = float(pt_array[i,0])
        psy = float(pt_array[i,1])
        pt = ogr.Geometry(ogr.wkbPoint)
        pt.AddPoint_2D(psx,psy)
        pt.Transform( in2ll )
        outrow = [ pt.GetY(), pt.GetX() ]
        outlist.append( outrow )
        # Write point data to CSV-file
        if debug > 0:
            fout.write( ("%f,%f\n" % (pt.GetX(),pt.GetY())) )
    
    # Close CSV-file
    if debug > 0:
        fout.close()
    
    return outlist