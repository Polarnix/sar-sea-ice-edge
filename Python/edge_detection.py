#!/usr/bin/python

# Name:          edge_detection.py
# Purpose:       Routines to detect sea ice edge.
# Author(s):     Nick Hughes
# Created:       2017-vi-26
# Modifications: 20??-??-?  - ?
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.818229
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys
import subprocess

import numpy as N

import osgeo.gdal as gdal

from skimage import feature
from skimage import morphology

from copy_gcps import copy_gcps
from memory_usage_psutil import memory_usage_psutil


# Local settings
TMPDIR="../tmp"
GDALHOME="/usr/local"


# Main edge detection function
def edge_detection( sarfn, landfn, debug ):
    if debug > 0:
        print '\n   Performing edge detection.'

    # Get root filename
    indir, infn = os.path.split(sarfn)
    rootfn = infn.replace('.tif','')
    rootfn = rootfn.replace('_sigma0_denoised','')

    # Set up GeoTIFF driver
    tifdrv = gdal.GetDriverByName('GTiff')

    # Define step size for loading data
    stepsz = 500

    # Open SAR backscatter and landmask files for input
    sar_ds = gdal.Open( sarfn )
    land_ds = gdal.Open( landfn )
    sar_geotrans = sar_ds.GetGeoTransform()
    sar_srs = sar_ds.GetProjection()
    nbands = sar_ds.RasterCount
    sar_gcps = sar_ds.GetGCPs()
    if sar_gcps[0].GCPY > 30.0:
        region = 'arctic'
        psorig = '90n'
    elif sar_gcps[0].GCPY < -30.0:
        region = 'antarctic'
        psorig = '90s'
    else:
        print ("Latitude is %f. Unhandled region in low latitudes." % sar_gcps[0].GCPY)
        sys.exit()

    # Get input band names
    outlist = []
    for i in range(nbands):
        # Need to get  band metadata
        # From S1 processed by Nansat, this is  provided
        #    name=sigma0_HV
        #    NETCDF_VARNAME=sigma0_HV
        #    polarization=HV
        # What to do if not provided?
        sar_band = sar_ds.GetRasterBand(i+1)
        sar_meta = sar_band.GetMetadata()
        try:
            # polstring = inmeta['polarization']
            bandname = sar_meta['name']
            polstring = bandname.split('_')[1]
            outlist.append(polstring)
        except:
            print "Unable to extract band name from image."
            sys.exit()
    # print outlist
    
    # sar_band = sar_ds.GetRasterBand(1)
    land_band = land_ds.GetRasterBand(1)

    # Get image size information
    xsz = land_band.XSize
    ysz = land_band.YSize

    # Create output images
    outtiffn = ("%s/%s_edge_detected.tif" % (TMPDIR,rootfn))
    outtifds = tifdrv.Create(outtiffn,xsz,ysz,1,gdal.GDT_Byte)
    outtifds.SetGeoTransform(sar_geotrans)
    outtifds.SetProjection(sar_srs)
    outband1 = outtifds.GetRasterBand(1)

    # Copy GCP points
    copy_gcps( sar_ds, outtifds )

    # Masking for edges of satellite images
    imgtype = 'S1'

    # Construct mask array to apply to lower edge of image
    bottom_width = [ [2980, 438], \
                     [4879, 330], \
                     [6960, 234], \
                     [8835, 133] ]
    lows1mask = N.ones( (500,xsz), dtype=N.uint8 )
    for i in range(len(bottom_width)):
        xextn = bottom_width[i][0]
        ymarg = bottom_width[i][1]
        lows1mask[-ymarg:,0:xextn] = 0
    lows1mask[-22:,:] = 0

    # Loop through blocks of data
    xblocks = int( xsz / stepsz ) + 1
    strx = 0
    idx = 0
    whileend = xsz

    if debug > 0:
        print "\n  #  strx  endx xcount  Memory"

    # while strx < 3000:
    while strx < whileend:
        endx = strx + stepsz
        if endx > xsz:
            endx = xsz
        xcount = (endx - strx)

        # Blank ice array
        ice = N.zeros( (ysz,xcount), dtype=N.uint8)

        # Load land mask data
        landmsk = land_ds.GetRasterBand(1).ReadAsArray(strx,0,xcount,ysz).astype(N.uint8)
        
        # Channel detection values
        detect_vals = { "HH": [  2, 0.20, 6.0 ], \
                        "VV": [  4, 0.20, 6.0 ], \
                        "HV": [  8, 0.01, 5.0 ], \
                        "VH": [ 16, 0.01, 5.0 ] }

        # Loop through SAR channels
        for i in range(nbands):
            # print outlist[i]
            sar_data = sar_ds.GetRasterBand(i+1).ReadAsArray(strx,0,xcount,ysz).astype(N.float32)
            # sar_data[N.isnan(sar_data)] = detect_vals[bandid][1]
            sar_data = N.power(10.0,sar_data/10.0)
            sar_data[N.isnan(sar_data)] = 0.0
            # print N.min(sar_data), N.max(sar_data)

            # Canny filter from scikit-image
            bandid = outlist[i]
            sar_min = 0.0
            sar_max = detect_vals[bandid][1]
            # print sar_min, sar_max
            scalar = 1.0 / (sar_max - sar_min)
            # print scalar
            sar_test = (sar_data - sar_min) * scalar
            # print N.min(sar_test), N.max(sar_test)
            sar_idx = N.nonzero( sar_test > 1.0 )
            sar_test[sar_idx] = 1.0
            # print N.min(sar_test), N.max(sar_test)
            # print N.average(sar_test)
            edgemsk = N.zeros( (ysz,xcount), dtype=N.bool)
            mskidx = N.nonzero(landmsk==0)
            edgemsk[mskidx] = True

            sigma_val = detect_vals[bandid][2]
            # print sigma_val
            rawedges = feature.canny(sar_test,sigma=sigma_val,mask=edgemsk,\
                low_threshold=0.05,high_threshold=0.15)
            # print N.unique(rawedges)
            edges = morphology.skeletonize(rawedges)
            # print N.unique(edges)
            # print N.min(edges), N.max(edges)
            iceidx = N.nonzero( edges == True )
            # print iceidx
            ice[iceidx] = ice[iceidx] + detect_vals[bandid][0]
            # print N.unique(ice)
            # print ' '
            
            sar_data = None

        # Mask by land
        mskidx = N.nonzero( landmsk > 0 )
        ice[mskidx] = 0

        # Mask image edges
        if imgtype == 'RS2':
            xmargin = 100
            ymargin = 50
            ice[0:ymargin,:] = 0
            ice[-ymargin:,:] = 0
            if strx == 0:
                ice[:,0:xmargin] = 0
            if endx == xsz:
                ice[:,-xmargin:] = 0
        elif imgtype == 'S1':
            # Lower edge
            ice[-500:,] = ice[-500:,] * lows1mask[:,strx:endx]
            # Left edge
            if strx == 0:
                ice[:,0:30] = 0
            # Right edge
            if endx == xsz:
                ice[:,-25:] = 0
            
        else:
            print ("Unknown satellite image type: %s" % imgtype)
            sys.exit()

        if debug > 0:
            print ("%3d %5d %5d %6d %7.2f" % (idx, strx, endx, xcount, memory_usage_psutil()))
            # print '        ', N.unique(ice)
        
        # Write ice array to files
        outband1.WriteArray(ice,strx,0)
        outband1.FlushCache()

        # if endx < xsz:
        #     strx = endx
        # else:
        strx = endx
        idx = idx + 1

    # Close input datasets
    sar_ds = None

    # Close output dataset
    outtifds = None

    # Warp images to Polar Stereographic
    if debug > 1:
        pstereofn = ("%s/%s_edge_detected_pstereo.tif" % (TMPDIR,rootfn))
        # print pstereofn
        projstr = ("+proj=stere +lat_0=%s +lon_0=0e +lat_ts=%s +ellps=WGS84 +datum=WGS84" \
            % (psorig,psorig))
        cmd = ("%s/bin/gdalwarp -of GTiff -ot Byte -co \"BIGTIFF=YES\"" % GDALHOME)
        cmd = ("%s -t_srs \"%s\" -tr 50 50" % (cmd,projstr))
        cmd = ("%s -r near -order 3 -overwrite -q %s %s" % (cmd,outtiffn,pstereofn)) 
        # print cmd
        try:
            retcode = subprocess.call(cmd, shell=True)
            if retcode < 0:
                print >>sys.stderr, "Child was terminated by signal", -retcode
        except OSError, e:
            print >>sys.stderr, "Execution failed:", e
            print cmd

    return outtiffn