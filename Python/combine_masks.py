#!/usr/bin/python

# Name:          combine_masks.py
# Purpose:       Routine to combine land and ice masks, plus fill in gaps using HH data.
# Author(s):     Nick Hughes
# Created:       2017-vi-26
# Modifications: 2017-vii-03 - Add output directory functionality.
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.818229
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys
import subprocess

import osgeo.gdal as gdal
import osgeo.ogr as ogr
import osgeo.osr as osr

import numpy as N

from PIL import Image, ImageDraw

from copy_gcps import copy_gcps
from memory_usage_psutil import memory_usage_psutil


# Local settings
TMPDIR="../tmp"
GDALHOME='/usr/local/bin'


# Main combine mask routine
#   land_mask_fn  = Land mask raster with same extents as satellite image (0 = sea, 1 = land)
#   ice_polygon_wkts = Set of polygon wkts providing areas known to be sea ice covered
#   copol_point_list = List of detected points from co-pol channel(s)
def combine_masks( land_mask_fn, ice_polygon_wkts, copol_point_list, outdir, debug ):
    
    if debug > 0:
        print '\n   Combining land and ice masks.'

    # Get root filename
    indir, infn = os.path.split(land_mask_fn)
    rootfn = infn.replace('.tif','')
    rootfn = rootfn.replace('_landmask','')
    print rootfn

    # 1. Read image parameters

    # Open input file
    inds = gdal.Open( land_mask_fn )
    transform = inds.GetGeoTransform()
    projstr = inds.GetProjection()
    gcps = inds.GetGCPs()
    nrow = inds.RasterYSize
    ncol = inds.RasterXSize
    # Create coordinate transformation from image coords to lat/lon
    tr = gdal.Transformer(inds, None, ['METHOD=GCP_POLYNOMIAL'])
    inds = None

    # Get region for projection        
    if gcps[0].GCPY > 30.0:
        region = 'arctic'
        psorig = '90n'
    elif gcps[0].GCPY < -30.0:
        region = 'antarctic'
        psorig = '90s'
    else:
        print ("Latitude is %f. Unhandled region in low latitudes." % gcps[0].GCPY)
        sys.exit()
    ps_projstr = ("+proj=stere +lat_0=%s +lon_0=0e +lat_ts=%s +ellps=WGS84 +datum=WGS84" \
        % (psorig,psorig))
    
    # 2. Create overall extent polygon using GCPS
    # 'GCPLine', 'GCPPixel', 'GCPX', 'GCPY', 'GCPZ'
    # print nrow, ncol
    top_list = []
    right_list = []
    bottom_list = []
    left_list = []
    for gcp in gcps:
        # print gcp
        if gcp.GCPLine == 0.0:
            top_list.append( [ gcp.GCPX, gcp.GCPY ] )
            # print 'top'
        elif gcp.GCPLine > (nrow - 2):
            bottom_list.append( [ gcp.GCPX, gcp.GCPY ] )
            # print 'bottom'
        if gcp.GCPPixel == 0.0:
            left_list.append( [ gcp.GCPX, gcp.GCPY ] )
            # print 'left'
        elif gcp.GCPPixel > (ncol -2):
            right_list.append( [ gcp.GCPX, gcp.GCPY ] )
            # print 'right'
    coord_list = top_list + right_list[1:] + bottom_list[-1::-1] + left_list[-1::-1]
    # print coord_list                    
    ring = ogr.Geometry(ogr.wkbLinearRing)
    for coord in coord_list:
        ring.AddPoint_2D( coord[0], coord[1] )
    coverage = ogr.Geometry(ogr.wkbPolygon)
    coverage.AddGeometry(ring)
    # print coverage.ExportToWkt()

    # Map projections
    ll_srs = osr.SpatialReference()
    ll_srs.ImportFromEPSG(4326)
    ps_srs = osr.SpatialReference()
    ps_srs.ImportFromProj4(ps_projstr)
    ll2ps = osr.CoordinateTransformation( ll_srs, ps_srs )
    ps2ll = osr.CoordinateTransformation( ps_srs, ll_srs )
    
    # Use ice polygon WKTs to clip coverage (Difference)
    clipped_poly = coverage.Clone()
    ncluster = len(ice_polygon_wkts)
    for i in range(ncluster):
        icepoly = ogr.CreateGeometryFromWkt(ice_polygon_wkts[i])
        # Buffer required, so convert to pstereo, apply buffer, and convert back
        icepoly.Transform(ll2ps)
        icepoly = icepoly.Buffer(500.0)   # Originally 1500.0
        icepoly.Transform(ps2ll)
        coverage = coverage.Difference( icepoly )
        
    # If debug, output a Shapefile with the coverage
    if debug > 0:
        shpdrv = ogr.GetDriverByName('ESRI Shapefile')
        outfn = ("%s/%s_polygon_coverage.shp" % (TMPDIR,rootfn))
        print outfn        
        if os.path.exists(outfn):
            shpdrv.DeleteDataSource(outfn)
        out_ds = shpdrv.CreateDataSource(outfn)
        layer_name = "polygon_coverage"
        out_lay = out_ds.CreateLayer(layer_name, ll_srs, geom_type=ogr.wkbMultiPolygon)
        idField = ogr.FieldDefn("cluster", ogr.OFTInteger)
        out_lay.CreateField(idField)
        featureDefn = out_lay.GetLayerDefn()
        
        out_feat = ogr.Feature(featureDefn)
        out_feat.SetGeometry(coverage)
        out_feat.SetField("cluster", 0 )
        out_lay.CreateFeature(out_feat)
        out_feat = None
        out_ds = None

     
    # Split remaining coverage from multi- to single parts
    polys = []
    if coverage.GetGeometryName() == 'POLYGON':
        polys.append( coverage.Clone() )
    elif coverage.GetGeometryName() == 'MULTIPOLYGON':
        npolys = coverage.GetGeometryCount()
        for i in range(npolys):
            subpoly = coverage.GetGeometryRef(i)
            polys.append( subpoly.Clone() )
            
    # Calculate polygon areas (in km2)
    poly_areas = []
    for i in range(npolys):
        tmppoly = polys[i].Clone()
        tmppoly.Transform(ll2ps)
        tmparea = tmppoly.Area() / (1000.0*1000.0)
        # print tmparea
        poly_areas.append( tmparea )
    poly_areas = N.array( poly_areas, dtype=N.float32 )
    
    # Count copol detections in polygons
    
    # Generate multipoint geometry
    # Clip by polygon
    # Count sub-geometries in result.
    
    npoint = len(copol_point_list)
    if debug > 0:
        print ("Number of co-pol points: %d" % npoint)
    counter = N.zeros( npolys, dtype=N.uint32 )
    multipt = ogr.Geometry(ogr.wkbMultiPoint)
    for i in range(npoint): 
        if debug > 0 and i % 100000 == 0:
            print i
        pt = ogr.Geometry(ogr.wkbPoint)
        pt.AddPoint_2D( copol_point_list[i][1], copol_point_list[i][0] )
        multipt.AddGeometry(pt)
    for i in range(npolys):
        subset = multipt.Intersection(polys[i])
        # print i, poly_areas[i], subset.GetGeometryCount()
        counter[i] = subset.GetGeometryCount()
    
    # Calculate point density and identify if it is ice or water
    density = N.zeros( npolys, dtype=N.float32 )
    # Threshold is points/km
    threshold = 1.0
    water_poly = []
    if debug > 0:
        print '  #   Area_km2    N_pts Density Identified_Type'
    for i in range(npolys):
        if counter[i] > 0:
            density[i] = float(counter[i]) / poly_areas[i]
        else:
            density[i] = -99.99
        if density[i] < threshold:
            polytype = 'water'
            water_poly.append( polys[i].Clone() )
        else:
            polytype = 'ice'
        if debug > 0:
            print ("%3d %10.2f %8d %7.2f %s" % ((i+1),poly_areas[i],counter[i],density[i],polytype))                 

    # 4. Generate new mask file

    # Mask colour value
    maskcol = 128
    
    # Create initial mask image
    mask = Image.new('L', (ncol, nrow), 0)

    # Loop through all the polygons
    points = []
    for i in range(len(water_poly)):
        # Get current polygon
        ring = water_poly[i].GetGeometryRef(0)
        # npoint = ring.GetPointCount()

        # Loop through polygon points and convert lat/lon to pixel coordinates
        points = []
        for idx, p in enumerate(ring.GetPoints()):
            # The trick is this line. The first 1 asks the transformer to do an
            # inverse transformation, so this is transforms lat/lon coords to pixel coords
            (success, point) = tr.TransformPoint(1, p[0], p[1], 0)
            if success:
                px = point[0]
                py = point[1]
                points.append((px, py))

        # Rasterize the polygon in image coordinates.
        # The polygon function requires at least two points.
        if len(points) > 2:
            ImageDraw.Draw(mask).polygon(points, outline=maskcol, fill=maskcol)

    # Convert to a numpy array (not sure if this is necessary)
    mask = N.array(mask).astype(bool)

    # Write output to file
    driver = gdal.GetDriverByName('GTiff')
    driver.Register()
    tmpmaskfn = ("%s/%s_owmask_tmp.tif" % (TMPDIR,rootfn))
    if debug > 0:
        print tmpmaskfn
    createopts=[ "COMPRESS=LZW", "BIGTIFF=YES"]
    outds = driver.Create(tmpmaskfn, ncol, nrow, 1, gdal.GDT_Byte, createopts)
    if outds is None:
        print 'Could not create ', tmpmaskfn
        sys.exit()
    outband = outds.GetRasterBand(1)
    outband.WriteArray(mask)

    # Flush data to disk
    outband.FlushCache()

    # Georeference the image and set the projection
    outds.SetGeoTransform(transform)
    outds.SetProjection(projstr)

    # Close and clean up
    outds = None
    inds = None

    # Apply GCPs
    maskfn = ("%s/%s_owmask.tif" % (TMPDIR,rootfn))
    if debug > 0:
        print maskfn
    cmd = ("%s/gdal_translate -of GTiff" % GDALHOME)
    cmd = ("%s -a_srs \"+proj=longlat +ellps=WGS84\"" % cmd)
    if debug == 0:
        cmd = ("%s -quiet" % cmd)
    for gcp in gcps:
        cmd = ("%s -gcp %f %f %f %f %f" \
            % (cmd,gcp.GCPPixel,gcp.GCPLine,gcp.GCPX,gcp.GCPY,gcp.GCPZ))
    cmd = ("%s %s %s" % (cmd,tmpmaskfn,maskfn))
    # print cmd
    try:
        retcode = subprocess.call(cmd, shell=True)
        if retcode < 0:
            print >>sys.stderr, "Child was terminated by signal", -retcode
    except OSError, e:
        print >>sys.stderr, "Execution failed:", e

    # Remove temporary file
    os.remove( tmpmaskfn )
    
    # 5. Combine new mask file with landmask

    # Open input file
    in1ds = gdal.Open( maskfn )
    transform = in1ds.GetGeoTransform()
    projstr = in1ds.GetProjection()
    gcps = in1ds.GetGCPs()
    ysz = in1ds.RasterYSize
    xsz = in1ds.RasterXSize

    in2ds = gdal.Open( land_mask_fn )
    
    # Set up GeoTIFF driver
    tifdrv = gdal.GetDriverByName('GTiff')

    outtiffn = ("%s/%s_mask.tif" % (outdir,rootfn))
    print outtiffn
    outtifds = tifdrv.Create(outtiffn,xsz,ysz,1,gdal.GDT_Byte)
    outtifds.SetGeoTransform(transform)
    outtifds.SetProjection(projstr)
    outband1 = outtifds.GetRasterBand(1)

    # Copy GCP points
    copy_gcps( in1ds, outtifds )


    # Define step size for loading data
    stepsz = 500

    # Loop through blocks of data
    xblocks = int( xsz / stepsz ) + 1
    strx = 0
    idx = 0
    whileend = xsz

    if debug > 0:
        print ("\nCalculating combined mask image\n")
        print "  #  strx  endx xcount  Memory"

    # while strx < 3000:
    while strx < whileend:
        endx = strx + stepsz
        if endx > xsz:
            endx = xsz
        xcount = (endx - strx)

        # Load data
        land_data = in1ds.GetRasterBand(1).ReadAsArray(strx,0,xcount,ysz).astype(N.uint16)
        ow_data = in2ds.GetRasterBand(1).ReadAsArray(strx,0,xcount,ysz).astype(N.uint16)

        # Combined mask array
        combined = land_data + ow_data

        # Write ice array to files
        outband1.WriteArray(combined,strx,0)
        outband1.FlushCache()

        if debug > 0:
            print ("%3d %5d %5d %6d %7.2f" % (idx, strx, endx, xcount, memory_usage_psutil()))

        # if endx < xsz:
        #     strx = endx
        # else:
        strx = endx
        idx = idx + 1

    # Close input datasets
    in1ds = None
    in2ds = None

    # Close output dataset
    outtifds = None
    
    # Remove open water mask file
    os.remove(maskfn)
            
    return outtiffn


# Main program routine for testing
if __name__ == '__main__':

    # Define land mask GeoTIFF filename     
    land_mask_fn = './landmask.tif'

    # Load OGR Shapefile driver
    shpdrv = ogr.GetDriverByName('ESRI Shapefile')
     
    # Load ice polygon wkts
    ice_polygon_wkts = []
    shpfn = './ice_polygons.shp'
    # shpfn = './cluster_polygons_longlat_buffered.shp'
    shp = ogr.Open(shpfn)
    layer = shp.GetLayer(0)
    nfeat = layer.GetFeatureCount()
    for i in range(nfeat):
        feat = layer.GetFeature(i)
        geometry = feat.GetGeometryRef()
        ice_polygon_wkts.append( geometry.ExportToWkt() )
    shp = None

    # Load copol_point_list
    copol_point_list = []
    shpfn = './/hh_detections.shp'
    shp = ogr.Open(shpfn)
    layer = shp.GetLayer(0)
    nfeat = layer.GetFeatureCount()
    for i in range(nfeat):
        feat = layer.GetFeature(i)
        geometry = feat.GetGeometryRef()
        lon = geometry.GetX()
        lat = geometry.GetY()
        copol_point_list.append( [ lat, lon ] )
    shp = None
    # print len(copol_point_list)
    
    # Call routine
    outdir = './'
    maskfn = combine_masks( land_mask_fn, ice_polygon_wkts, copol_point_list, outdir, 1)
    print maskfn
    