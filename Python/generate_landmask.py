#!/usr/bin/python

# Name:          generate_landmask.py
# Purpose:       Standalone routine to generate landmask GeoTIFF.
# Author(s):     Nick Hughes
# Created:       2017-vi-26
# Modifications: 20??-??-?  - ?
# Copyright:     (c) Norwegian Meteorological Institute, 2017
# Citing:        https://doi.org/10.5281/zenodo.818229
#
# License:       This file is part of the BIFROST ice charting system.
#                BIFROST is free software: you can redistribute it and/or modify
#                it under the terms of the GNU General Public License as published by
#                the Free Software Foundation, version 3 of the License.
#                http://www.gnu.org/licenses/gpl-3.0.html
#                This program is distributed in the hope that it will be useful,
#                but WITHOUT ANY WARRANTY without even the implied warranty of
#                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

import os, sys
import shutil

from gshhs_landmask import gshhs_landmask

# Local settings
TMPDIR="../tmp"


def generate_landmask( sarfn, mask, debug ):
    if debug > 0:
        print '\n   Creating landmask image.'

    # Get root filename
    indir, infn = os.path.split(sarfn)
    rootfn = infn.replace('.tif','')

    # Construct a landmask
    gshhs_list = [ 'f', 'h', 'i', 'l', 'c' ]
    if mask.lower() in gshhs_list:
        maskfn = gshhs_landmask( sarfn, mask, debug )
    elif mask.lower() == 'a':
        # maskfn = add_landmask( sarfn, 1 )
        pass
    elif mask == '0':
        maskfn = None
    else:
        maskdir,maskroot = os.path.split(mask)
        maskfn = ("%s/%s_landmask.tif" % (TMPDIR,rootfn))
        shutil.copyfile( mask, maskfn )
    print maskfn
    
    return maskfn